#include <iostream>
#include <fstream>
#include <sstream>
#include <stdio.h>
#include <vector>
#include <math.h>
#include "TFile.h"
#include "TDirectory.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TMultiGraph.h"
#include "TH1.h"
#include "TH2.h"
#include "TF1.h"

using namespace std;

void ccpd_tuning_scan_files(){
  cout << "========== BEGENNING ==========" << endl;

  //gStyle->SetOptStat(0);

  bool tuned = false;					//      <====== CHANGE HERE

  int p;
  int chip = 2; 					//	<======  CHANGE HERE
  string chip_name[2] = {"Caribou04","Caribou05"};

  double inj_min = 0;
  //double inj_max;
  //const int inj_steps;
  if (tuned) {double inj_max = 0.3; const int inj_steps = 30;}
  if (!tuned) {double inj_max = 0.3; const int inj_steps = 30;}    //   <==== CHANGE HERE

  double inj_volts[inj_steps];
  double new_conv_fact = 8600;
  double old_conv_fact = 1660/.25;
  for(int v=1; v < inj_steps+1; v++) inj_volts[v]=(((inj_max-inj_min)/inj_steps)*v)*new_conv_fact;

  int pixel, cel, row;

  int pixel_nr=0;
  int sub_pixel=0;

  if (tuned) {
    const int tdacs = 1;
    int TDACs[tdacs] = {83};
  }
  if (!tuned) { 
    const int tdacs = 5;				//  	<======= CHANGE HERE and bellow
    //int TDACs[tdacs] = {1, 14};
    int TDACs[tdacs] = {1,5,7,11,15};
    //int TDACs[tdacs] = {1, 3, 5, 7, 9, 11, 13, 15};
    //int TDACs[tdacs] = {0, 500, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 5500, 6000};
    //int TDACs[tdacs] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
  }

  double dummy = 0;
  double threshold=0;
  double threshold_list[288];
  double pulses[inj_steps];

  TCanvas* c1 = new TCanvas();
  string root_file_name;
  if (tuned) root_file_name = "TestPulse_scan_TUNED_" + chip_name[chip-1] + ".root";
  if (!tuned) root_file_name = "TestPulse_scan_All_TDACs_" + chip_name[chip-1] + ".root";
  //if (tuned) TFile* root_file = new TFile(Form("TestPulse_scan_TUNED_%s.root",chip_name[chip-1]),"RECREATE");
  //if (!tuned) TFile* root_file = new TFile(Form("TestPulse_scan_All_TDACs_%s.root",chip_name[chip-1]),"RECREATE");
  TFile* root_file = new TFile(root_file_name.c_str(), "RECREATE");

  TDirectory* dir_TDAC[tdacs];
  for(int tdac=0; tdac < tdacs; tdac++) dir_TDAC[tdac] = root_file->mkdir(Form("TDAC_%d",TDACs[tdac]));
  TDirectory *dir_Super;  dir_Super= root_file->mkdir("Superpositions");
  if (!tuned) {TDirectory *dir_Inter;  dir_Inter= root_file->mkdir("Interpolation");}
  TDirectory *dir_thl_map;  dir_thl_map= root_file->mkdir("Threshold_Map");
  TDirectory *dir_Thres;  dir_Thres= root_file->mkdir("Threshold_dist");
  TDirectory *dir_Sigma;  dir_Sigma= root_file->mkdir("Thr_Sigma_dist");

  TH1D*  thl_dist[tdacs];	
  TH1D*  sig_dist[tdacs];	
  TH2D*   tp_scan[tdacs];
  TH2D*   thl_map[tdacs];
  TGraphErrors* pixel_scan[288];
  TGraph* s_curves[tdacs][288];
  TMultiGraph* s_curve_mg[tdacs];
  for(int tdac=0; tdac<tdacs; tdac++){
    s_curve_mg[tdac] = new TMultiGraph(Form("Scurve_superposed_TDAC_%d",TDACs[tdac]),Form("Superposition_TDAC_%d",TDACs[tdac]));
    tp_scan[tdac] = new TH2D(Form("TestPulse_scan_TDAC_%d", TDACs[tdac]), 
		             Form("TestPulse_scan_TDAC_%d", TDACs[tdac]),
			     288, 288, 575, inj_steps, inj_min, inj_max*new_conv_fact);
    tp_scan[tdac]->SetAxisRange(0,100,"Z");
    thl_map[tdac] = new TH2D(Form("Threshold_MAP_TDAC_%d", TDACs[tdac]), Form("Threshold_MAP_TDAC_%d", TDACs[tdac]), 60,-0.5,59.5,12,-0.5,11.5);
    thl_map[tdac]->GetZaxis()->SetRangeUser(0,inj_max*new_conv_fact);
    thl_dist[tdac] = new TH1D(Form("THL_dist_TDAC_%d",TDACs[tdac]),Form("THL_dist_TDAC_%d",TDACs[tdac]), 200, 0, inj_max*new_conv_fact);
    sig_dist[tdac] = new TH1D(Form("THL_sigma_dist_TDAC_%d",TDACs[tdac]),Form("THL_sigma_dist_TDAC_%d",TDACs[tdac]), 300, 0, 300);
  }
  TH2D* number_map = new TH2D("number_map","number_map;Pixel Col;Pixel Row", 60,-0.5,59.5,12,-0.5,11.5);

  int row2,col, pix;
  int pixel_number;
  int sub_col, sub_row;
  for(col=0;col<20;col++){
    for(row2=0;row2<6;row2++){
      for(pix=0;pix<6;pix++){
        sub_col = col*3 + pix%3;
        if(row2%2==0) sub_row = row2*2 +1 -pix/3;
        if(row2%2==1) sub_row = row2*2 + pix/3;
        pixel_number = col*36 + row2*6 + pix;
        sub_col = col*3 + pix%3;
        if(row2%2==0) sub_row = row2*2 +1 -pix/3;
        if(row2%2==1) sub_row = row2*2 + pix/3;
        pixel_number = col*36 + row2*6 + pix;
	number_map->Fill(sub_col,sub_row,pixel_number);
      }
    }
  }


  if (!tuned){
    TMultiGraph* pixel_scan_mg = new TMultiGraph("Pixel_Scan_Superposition", "Pixel_Scan_Superposition");
    TH2D* pixel_scan_super = new TH2D("Pixel_Scan_Super", "Pixel_Scan_Super", 14, 1, 14, 50, 0, inj_max*new_conv_fact);
    target_thl_map = new TH2D("Target_Threshold_MAP", "Target_Threshold_MAP", 60,-0.5,59.5,12,-0.5,11.5);
    target_thl_map->GetZaxis()->SetRangeUser(0,inj_max*new_conv_fact);
    target_thl_map_dif = new TH2D("Target_Threshold_DIF_MAP", "Target_Threshold_DIF_MAP", 60,-0.5,59.5,12,-0.5,11.5);
    target_thl_map_dif->GetZaxis()->SetRangeUser(-500,500);
    TH1D* target_thl_dist = new TH1D("Target_THL_dist","Target_THL_dist", 100, 0, inj_max*new_conv_fact);
    TH1D* target_thl_dist_dif = new TH1D("Target_THL_DIF_dist","Target_THL_DIF_dist", 100, -500, 500);
    TGraphErrors* threshold_evol = new TGraphErrors(tdacs);
    TGraphErrors* sigma_evol = new TGraphErrors(tdacs);
  }

  for(int q=0; q<288; q++) pixel_scan[q] = new TGraphErrors();

  TF1 *f1 = new TF1("erf","[2]*TMath::Erf([0]*(x-[1]))+[3]",inj_min, inj_max*new_conv_fact);
  f1->SetParameter(1,500);  f1->SetParLimits(1,0,inj_max*new_conv_fact);  f1->SetParName(1, "Mean X");
  f1->SetParameter(2,50);
  f1->SetParameter(3,50);  f1->SetParLimits(3,45,55);
  f1->SetParLimits(0,0,0.02);
  f1->SetParName(0,"sigma");

  ifstream input_file;
  string file_line;
  string file;
  for(int tdac=0; tdac<tdacs; tdac++){
    stringstream file_name;
    if(tuned) file_name << "Interpolation_tuning_scan_data_a" << chip << "_TDAC_" << TDACs[tdac] << ".txt";
    if(!tuned) file_name << "Interpolation_tuning_scan_data_a" << chip << "_TDAC_" << TDACs[tdac] << ".txt";
    file = file_name.str();
    cout << "Reading file: " << file << endl;

    pixel=0;
    input_file.open(file.c_str());
    while(getline(input_file,file_line)){
      stringstream mystream(file_line);
      p=0;
      while(mystream >> dummy){
	if (p==0) cel = dummy;	if (p==1) row = dummy;	if (p==2) sub_pixel = dummy;
	if (p>2)  {
	  pulses[p-3]=dummy;
	  //if(dummy<101) pulses[p-3]=dummy; if(dummy>101) pulses[p-3]=100;
          pixel_nr=cel*36+row*6+sub_pixel;
	  tp_scan[tdac]->Fill(pixel_nr, (((inj_max-inj_min)/inj_steps)*(p-3))*new_conv_fact, dummy);
	}
        p++;
      }
      s_curves[tdac][pixel] = new TGraph(inj_steps, inj_volts, pulses);
      s_curves[tdac][pixel]->SetName(Form("S_Curve_TDAC_%d_Pixel_%d", TDACs[tdac], pixel_nr));
      s_curves[tdac][pixel]->Fit(f1,"Q");
      threshold = f1->GetParameter(1);
      threshold_list[pixel]=threshold;
      s_curve_mg[tdac]->Add(s_curves[tdac][pixel]);

      if (!tuned){
        pixel_scan[pixel]->SetPoint(tdac, TDACs[tdac], threshold);
        pixel_scan[pixel]->SetPointError(tdac, 0, 10);
        pixel_scan_super->Fill(TDACs[tdac],threshold);
        if(tdac==tdacs-1) {
          //dir_Inter->cd();
	  pixel_scan_mg->Add(pixel_scan[pixel]);
	  //pixel_scan[pixel]->SetTitle(Form("Pixel_%d_scan",pixel_nr));
	  pixel_scan[pixel]->SetName(Form("Pixel_%d_scan",pixel_nr));
    	  pixel_scan[pixel]->Fit("pol1","Q");
          //pixel_scan[pixel]->Write();
        }
      }
      thl_dist[tdac]->Fill(threshold);
      sig_dist[tdac]->Fill(sqrt(2.)/(2*f1->GetParameter(0)));

      dir_TDAC[tdac]->cd();
      s_curves[tdac][pixel]->Write();

      f1->SetParameter(1,500); f1->SetParLimits(1,0,inj_max*new_conv_fact);
      f1->SetParameter(2,50);
      f1->SetParameter(3,50);  f1->SetParLimits(3,45,55);
      f1->SetParLimits(0,0,0.02);

      pixel++;
    }

    dir_Super->cd();
    s_curve_mg[tdac]->Write();
    tp_scan[tdac]->Write();
    tp_scan[tdac]->ProjectionY()->Write();

    dir_Thres->cd();
    thl_dist[tdac]->Fit("gaus","Q");
    thl_dist[tdac]->Write();

    for(int n=0; n<288; n++){
      for(int x=1; x<61; x++){
        for(int y=1; y<13; y++){
          if(n+288 == number_map->GetBinContent(x,y)){
            thl_map[tdac]->SetBinContent(x,y,threshold_list[n]);
          }
        }
      }
    }

    dir_thl_map->cd();
    thl_map[tdac]->Write();

    dir_Sigma->cd();
    sig_dist[tdac]->Fit("gaus","Q");
    sig_dist[tdac]->Write();


    if (!tuned){
      /*
      c1->cd();
      tp_scan[tdac]->Draw("COLZ");
      c1->Print("TP_Scan.gif+50");
      c1->Clear();
      s_curve_mg[tdac]->Draw("A");
      c1->Print("SCurves.gif+50");
      c1->Clear();
      s_curve_mg[tdac]->GetYaxis()->SetRangeUser(0,110);
      s_curve_mg[tdac]->Draw("A");
      c1->Print("SCurves_2.gif+50");

      c1->cd();
      thl_dist[tdac]->Draw();
      c1->Print("THL_Dist.gif+50");
      c1->Clear();

      c1->cd();
      c1->Clear();
      thl_map[tdac]->Draw("COLZ");
      c1->Print("THL_MAP.gif+50");
      //*/
      threshold_evol->SetPoint(tdac, TDACs[tdac],thl_dist[tdac]->GetFunction("gaus")->GetParameter(1));
      threshold_evol->SetPointError(tdac, 0, thl_dist[tdac]->GetFunction("gaus")->GetParameter(2));
      sigma_evol->SetPoint(tdac, TDACs[tdac], sig_dist[tdac]->GetFunction("gaus")->GetParameter(1));
      sigma_evol->SetPointError(tdac, 0, sig_dist[tdac]->GetFunction("gaus")->GetParameter(2));
    }

    input_file.close();
  }

  if (!tuned){
    dir_Super->cd();
    pixel_scan_mg->Write();
    pixel_scan_super->Write();

    threshold_evol->SetName("Matrix_Threshold_Evolution");
    threshold_evol->Fit("pol1","Q");
    threshold_evol->Write();

    sigma_evol->SetName("Matrix_Equivalent_Noise_Charge_Evolution");
    sigma_evol->Fit("pol1","Q");
    sigma_evol->Write();

    double offset = threshold_evol->GetFunction("pol1")->GetParameter(0);
    double slope  = threshold_evol->GetFunction("pol1")->GetParameter(1);
    double Target = 7*slope + offset;
    //double Target = 820;

    cout << endl << "	===> Target threshold: " << Target << endl << endl;

    double dummy_dif=500;
    int target_tdac[288];
    int target_tdac2[288];
    int target_thl[288];
    for(pixel=0; pixel<288; pixel++){
      double offset_pixel = pixel_scan[pixel]->GetFunction("pol1")->GetParameter(0);
      double slope_pixel  = pixel_scan[pixel]->GetFunction("pol1")->GetParameter(1);
      target_tdac[pixel] = floor((Target-offset_pixel)/slope_pixel + 0.5);
      if(target_tdac[pixel]<1) target_tdac[pixel]=1;
      if(target_tdac[pixel]>15) target_tdac[pixel]=15;

      dummy_dif=5000;
      for(int x=1; x<16; x++){
        if (dummy_dif > abs(Target - pixel_scan[pixel]->Eval(x,0,""))){
          dummy_dif = abs(Target - pixel_scan[pixel]->Eval(x,0,""));
          target_tdac2[pixel]=x;
        }
      }
      cout << pixel+288 << " " << target_tdac2[pixel] << endl;
      pixel_scan[pixel]->SetTitle(Form("Pixel %d | TDAC	%d (THL=%.1f) for Target THL %.1f",
			                pixel+288, target_tdac2[pixel], pixel_scan[pixel]->Eval(target_tdac2[pixel],0,""), Target));
      target_thl[pixel] = pixel_scan[pixel]->Eval(target_tdac2[pixel],0,"");
      target_thl_dist->Fill(pixel_scan[pixel]->Eval(target_tdac2[pixel],0,""));
      target_thl_dist_dif->Fill(Target-(pixel_scan[pixel]->Eval(target_tdac2[pixel],0,"")));
      dir_Inter->cd();
      pixel_scan[pixel]->Write();
    }

    TH2D* TDAC_map = new TH2D("TDAC_map","TDAC_map;Pixel Col;Pixel Row", 60,-0.5,59.5,12,-0.5,11.5);
    TH2D* TDAC_map2 = new TH2D("TDAC_map2","TDAC_map2;Pixel Col;Pixel Row", 60,-0.5,59.5,12,-0.5,11.5);
    TH2D* TDAC_map_dif = new TH2D("TDAC_map_dif","TDAC_map_dif;Pixel Col;Pixel Row", 60,-0.5,59.5,12,-0.5,11.5);
    TDAC_map->GetZaxis()->SetRangeUser(-1,16);
    TDAC_map2->GetZaxis()->SetRangeUser(-1,16);
    TDAC_map_dif->GetZaxis()->SetRangeUser(-5,5);

    int x,y, pixelll=-1;

    for(x=1; x<61; x++){
      for(y=1; y<13; y++){
        TDAC_map2->SetBinContent(x,y,15);
      }
    }

    for(int n=0; n<288; n++){
      for(x=1; x<61; x++){
        for(y=1; y<13; y++){
          if(n+288 == number_map->GetBinContent(x,y)){
            TDAC_map->SetBinContent(x,y,target_tdac[n]);
            TDAC_map2->SetBinContent(x,y,target_tdac2[n]);
            TDAC_map_dif->SetBinContent(x,y,(target_tdac[n]-target_tdac2[n]));
	    target_thl_map->SetBinContent(x,y,target_thl[n]);
	    target_thl_map_dif->SetBinContent(x,y,Target-target_thl[n]);
          }
        }
      }
    }

    root_file->cd();
    number_map->Write();
    TDAC_map->Write();
    TDAC_map2->Write();
    TDAC_map_dif->Write();
    target_thl_map->Write();
    target_thl_map_dif->Write();
    target_thl_dist->Fit("gaus","Q");
    target_thl_dist->Write();
    target_thl_dist_dif->Write();

    string file_name2 = chip_name[chip-1] + "_TDAC_MAP.txt";
    ofstream output_file(file_name2.c_str());
    output_file << "[";
    for(int g=1;g<61;g++){
      output_file << "[ ";
      for(int h=1;h<13;h++){
        if(h==12) output_file << TDAC_map2->GetBinContent(g,h);
        else output_file << TDAC_map2->GetBinContent(g,h) << ", ";
      }
      if(g==60) output_file << "]";
      else output_file << "], ";
    }
    output_file << "]" << endl;
    output_file.close();
  }
  root_file->Close();

  cout << "============= END =============" << endl;
}
